/*   //<>//
 * Alumnos: Domínguez Quezada Miguel Alejandro.
 *          Estrada González Luis Humberto.
 *
 * Asignatura: Multimedia    Grupo: 3TM3
 * Profesor: Noé Sierra Romero.
 *
 */


/* Librerías */
import TUIO.*;
import processing.video.*;

TuioProcessing tuioClient;

/* Variables globales */
float cursor_size = 15;
float object_size = 60;
float table_size = 760;
float scale_factor = 1;
PFont font;
boolean verbose = false; // print console debug messages
boolean callback = true; // updates only after callbacks

boolean  flag1, flag2, flag3;
PImage fondo;
PImage gauss1;
PImage gauss2;
PImage gauss3;
PImage faraday1;
PImage faraday2;
PImage faraday3;
PImage maxwell1;
PImage maxwell2;
PImage maxwell3;
Movie gauss;
Movie faraday;
Movie maxwell;
PShape upper, inner, ring;

/* Configuración de la interfaz gráfica de usuario */
void setup() {
  
  /* Fondo */
  size(1340,690);
  fondo = loadImage("uni.jpg");
  background(fondo);
  
  /* Elementos multimedia */
  gauss = new Movie(this, "gauss.mp4");
  faraday = new Movie(this, "faraday.mov");
  maxwell = new Movie(this, "maxwell.mov");
  
  gauss1 = loadImage("gauss1.jpg");
  gauss2 = loadImage("gauss2.jpg");
  gauss3 = loadImage("gauss3.jpg");
  faraday1 = loadImage("faraday1.jpg");
  faraday2 = loadImage("faraday2.jpg");
  faraday3 = loadImage("faraday3.jpg");
  maxwell1 = loadImage("maxwell1.jpg");
  maxwell2 = loadImage("maxwell2.jpg");
  maxwell3 = loadImage("maxwell3.jpg");
  
  /*Construcción de figura central */
  ring = createShape(GROUP);
  upper = createShape(ELLIPSE,670,345,200,200);
  inner = createShape(ELLIPSE,670,345,170,170);
  upper.setFill(false);
  inner.setFill(false);
  upper.setStroke(255);
  inner.setStroke(255);
  ring.addChild(upper);
  ring.addChild(inner);
  
  if (!callback) {
    frameRate(60);
    loop();
  } else noLoop();
  
  font = createFont("Arial", 18);
  scale_factor = height/table_size;
  
  tuioClient  = new TuioProcessing(this);
}

// within the draw method we retrieve an ArrayList of type <TuioObject>, <TuioCursor> or <TuioBlob>
// from the TuioProcessing client and then loops over all lists to draw the graphical feedback.
void draw() {
  textFont(font,18*scale_factor);
  float obj_size = object_size*scale_factor; 
  float cur_size = cursor_size*scale_factor;
  
  //Pinta el fondo cada vez para que los objetos no se queden en la pantalla
  background(fondo);
  int noMarca = 0;
  //shape(ring);
     
  ArrayList<TuioObject> tuioObjectList = tuioClient.getTuioObjectList();
  for (int i=0;i<tuioObjectList.size();i++) {
     TuioObject tobj = tuioObjectList.get(i);
     
     /* Núcleo de las transiciones */
     
     //Obtenemos el número de marca asociado
     noMarca = tobj.getSymbolID();
     
     //Obtenemos el ángulo al que se encuentra dicha marca
     float anguloGrados = tobj.getAngle()*180/PI;
  
     //La función getInfo nos proporciona la información del científico acorde a la marca asociada
     getInfo(noMarca, obj_size, anguloGrados, tobj); 
   }
   
   ArrayList<TuioCursor> tuioCursorList = tuioClient.getTuioCursorList();
   for (int i=0;i<tuioCursorList.size();i++) {
      TuioCursor tcur = tuioCursorList.get(i);
      ArrayList<TuioPoint> pointList = tcur.getPath();
      
      if (pointList.size()>0) {
        stroke(0,0,255);
        TuioPoint start_point = pointList.get(0);
        for (int j=0;j<pointList.size();j++) {
           TuioPoint end_point = pointList.get(j);
           line(start_point.getScreenX(width),start_point.getScreenY(height),end_point.getScreenX(width),end_point.getScreenY(height));
           start_point = end_point;
        }
        
        stroke(192,192,192);
        ellipse( tcur.getScreenX(width), tcur.getScreenY(height),cur_size,cur_size);
        text(""+ tcur.getCursorID(),  tcur.getScreenX(width)-5,  tcur.getScreenY(height)+5);
      }
   }
   
  ArrayList<TuioBlob> tuioBlobList = tuioClient.getTuioBlobList();
  for (int i=0;i<tuioBlobList.size();i++) {
     TuioBlob tblb = tuioBlobList.get(i);
     stroke(0);
     pushMatrix();
     translate(tblb.getScreenX(width),tblb.getScreenY(height));
     rotate(tblb.getAngle());
     ellipse(-1*tblb.getScreenWidth(width)/2,-1*tblb.getScreenHeight(height)/2, tblb.getScreenWidth(width), tblb.getScreenWidth(width));
     popMatrix();
     text(""+tblb.getBlobID(), tblb.getScreenX(width), tblb.getScreenX(width));
   }
}

// --------------------------------------------------------------
// these callback methods are called whenever a TUIO event occurs
// there are three callbacks for add/set/del events for each object/cursor/blob type
// the final refresh callback marks the end of each TUIO frame

/* Llamada cuando un objeto entra a la escena */
void addTuioObject(TuioObject tobj) {
  if (verbose) println("add obj "+tobj.getSymbolID()+" ("+tobj.getSessionID()+") "+tobj.getX()+" "+tobj.getY()+" "+tobj.getAngle());
  
  /* Evaluamos si alguna marca entró a la escena y en caso de estarlo, su flag se vuelve verdadera */
  switch(tobj.getSymbolID()){
    case 1: flag1 = true;
            println("1 entró a la escena");
            break;
            
    case 2: flag2 = true; 
            println("2 entró a la escena");
            break;
            
    case 3: flag3 = true;
            println("3 entró a la escena");
            break;
    
    default: println("Marca no asociada");
  }
}

/* Llamada cuando un objeto es movido en la escena */
void updateTuioObject (TuioObject tobj) {
  if (verbose) println("set obj "+tobj.getSymbolID()+" ("+tobj.getSessionID()+") "+tobj.getX()+" "+tobj.getY()+" "+tobj.getAngle()
          +" "+tobj.getMotionSpeed()+" "+tobj.getRotationSpeed()+" "+tobj.getMotionAccel()+" "+tobj.getRotationAccel());
}

/* Llamada cuando un objeto es removido de la escena */
void removeTuioObject(TuioObject tobj) {
  if (verbose) println("del obj "+tobj.getSymbolID()+" ("+tobj.getSessionID()+")");
  
  /* Evaluamos cuando un objeto es retirado de la escena y cambiamos su flag a falso */
  switch(tobj.getSymbolID()){
    case 1: flag1 = false;
            gauss.stop();
            println("1 salió de escena");
            break;
            
    case 2: flag2 = false;
            faraday.stop();
            println("2 salió de escena");
            break;
            
    case 3: flag3 = false;
            maxwell.stop();
            println("3 salió de escena");
            break;
    
    default: gauss.stop(); faraday.stop(); maxwell.stop();
  } 
}

// --------------------------------------------------------------
// called when a cursor is added to the scene
void addTuioCursor(TuioCursor tcur) {
  if (verbose) println("add cur "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") " +tcur.getX()+" "+tcur.getY());
  //redraw();
}

// called when a cursor is moved
void updateTuioCursor (TuioCursor tcur) {
  if (verbose) println("set cur "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") " +tcur.getX()+" "+tcur.getY()
          +" "+tcur.getMotionSpeed()+" "+tcur.getMotionAccel());
  //redraw();
}

// called when a cursor is removed from the scene
void removeTuioCursor(TuioCursor tcur) {
  if (verbose) println("del cur "+tcur.getCursorID()+" ("+tcur.getSessionID()+")");
  //redraw()
}

// --------------------------------------------------------------
// called when a blob is added to the scene
void addTuioBlob(TuioBlob tblb) {
  if (verbose) println("add blb "+tblb.getBlobID()+" ("+tblb.getSessionID()+") "+tblb.getX()+" "+tblb.getY()+" "+tblb.getAngle()+" "+tblb.getWidth()+" "+tblb.getHeight()+" "+tblb.getArea());
  //redraw();
}

// called when a blob is moved
void updateTuioBlob (TuioBlob tblb) {
  if (verbose) println("set blb "+tblb.getBlobID()+" ("+tblb.getSessionID()+") "+tblb.getX()+" "+tblb.getY()+" "+tblb.getAngle()+" "+tblb.getWidth()+" "+tblb.getHeight()+" "+tblb.getArea()
          +" "+tblb.getMotionSpeed()+" "+tblb.getRotationSpeed()+" "+tblb.getMotionAccel()+" "+tblb.getRotationAccel());
  //redraw()
}

// called when a blob is removed from the scene
void removeTuioBlob(TuioBlob tblb) {
  if (verbose) println("del blb "+tblb.getBlobID()+" ("+tblb.getSessionID()+")");
  //redraw()
}

// --------------------------------------------------------------
// called at the end of each TUIO frame
void refresh(TuioTime frameTime) {
  if (verbose) println("frame #"+frameTime.getFrameID()+" ("+frameTime.getTotalMilliseconds()+")");
  if (callback) redraw();
}

void movieEvent(Movie m) 
{
  m.read();
}


/* En esta función de desgloza toda la información de los científicos */
/* Aquí evaluamos el ángulo de rotación y presentamos los diferentes contenidos de acuerdo a dicho ángulo */
void getInfo(int marca, float obj_size, float giro, TuioObject tobj) {
  int x1 = 40;
  int x2 = 850;
  /* Primer cientifico: Gauss */
  if(marca == 1 && flag1){
    textSize(32);
    text("Carl Friedrich Gauss", 500, 100);
    textSize(28);
    text("Brunswick, actual Alemania, 1777 - Gotinga, id., 1855",350,130);
    //Imágenes
    if(giro >= 0 && giro < 90){
        gauss.stop();
        pushMatrix();
        image(gauss1,50,50,300,300);
        image(gauss2,900,250,300,300);
        image(gauss3,300,450,180,200);
        popMatrix();
    //Reseña
    }else if(giro >= 90 && giro < 180){
      gauss.stop();
      pushMatrix();
      textSize(26);
      text("Breve Reseña", 560, 160);
      textSize(18);
      text("Gauss fue un matemático, físico y astrónomo alemán.\n", x1,280);
      text("Nacido en el seno de una familia humilde,\n",x1,310);
      text("desde muy temprana edad Gauss dio muestras\n",x1,340);
      text("de una prodigiosa capacidad para las matemáticas,\n",x2,280);
      text("hasta el punto de ser recomendado al duque de Brunswick\n",x2,310);
      text("por sus profesores de la escuela primaria.",x2,340);
      popMatrix();
    //Logros
    }else if(giro >= 180 && giro < 270){
      gauss.stop();
      pushMatrix();
      textSize(26);
      text("Principales Logros", 560, 160);
      textSize(18);
      text("Los trabajos de Gauss son muchísimos y tienen\n",x1,280);
      text("una influencia muy grande en la práctica\n",x1,310);
      text("totalidad de las ramas de  la Física y las Matemáticas\n",x1,340);
      text("A lo largo de su vida hizo varias aportaciones.",x1,370);
      text("Entre las que destacan:\n",x2,280);
      text("Teoría de los errores.\n",x2,310);
      text("Método general para la resolución de las ecuaciones.\n",x2,340);
      text("Teorema de Gauss en matemática.\n",x2,370);
      text("Ley de Gauss en electricidad.\n",x2,400);
      popMatrix();
    //Video
    }else if(giro >= 270 && giro < 360){
      gauss.play();
      gauss.read();
      movieEvent(gauss);
      pushMatrix();
      image(gauss, tobj.getScreenX(width),tobj.getScreenY(height),displayWidth/2, displayHeight/3);
      fill(255);
      popMatrix();
  }
  
  /* Segundo científico: Faraday */
  if(marca == 2 && flag2){
    textSize(32);
    text("Michael Faraday", 500, 100);
    textSize(28);
    text("Newington, Gran Bretaña, 1791 - Londres, 1867",350,130);
    if(giro >= 0 && giro < 90){
      faraday.stop();
      pushMatrix();
      image(faraday1,50,50,300,300);
      image(faraday2,900,250,300,300);
      image(faraday3,300,450,180,200);
      popMatrix();
    }else if(giro >= 90 && giro < 180){
      faraday.stop();
      pushMatrix();
      textSize(26);
      text("Breve Reseña", 560, 160);
      textSize(18);
      text("Científico británico, uno de los físicos.\n", x1,280);
      text("más destacados del siglo XIX. Michael Faraday\n",x1,310);
      text("nació en el seno de una familia humilde\n",x1,340);
      text("y recibió una educación básica. A temprana edad,\n",x2,280);
      text("tuvo que empezar a trabajar, primero como repartidor\n",x2,310);
      text("de periódicos, y a los catorce años en una librería,",x2,340);
      text("donde tuvo la oportunidad de leer algunos artículos",x2,370);
      text("científicos que lo impulsaron a realizar",x2,400);
      text("sus primeros experimentos.",x2,430);
      popMatrix();
    }else if(giro >= 180 && giro < 270){
      faraday.stop();
      pushMatrix();
      textSize(26);
      text("Principales Logros", 560, 160);
      textSize(18);
      text("Los descubrimientos de Faraday fueron determinantes en\n",x1,280);
      text("el avance que pronto iban a experimentar los\n",x1,310);
      text("estudios sobre el electromagnetismo.\n",x1,340);
      text("Posteriores aportaciones que resultaron definitivas",x1,370);
      text("para el desarrollo de la física, como",x2,280);
      text("es el caso de la teoría del campo electromagnético",x2,310);
      text("introducida por James Clerk Maxwell,",x2,340);
      text("se fundamentaron en la labor pionera",x2,370);
      text("que había llevado a cabo Michael Faraday.",x2,400);
      popMatrix();
    }else if(giro >= 270 && giro < 360){
      faraday.play();
      faraday.read();
      movieEvent(faraday);
      pushMatrix();
      image(faraday, tobj.getScreenX(width),tobj.getScreenY(height),displayWidth/2, displayHeight/3);
      fill(255);
      popMatrix();
    }
  }
  
  
  
  /* Tercer científico: Maxwell */
  if(marca == 3 && flag3){
    textSize(32);
    text("James Clerk Maxwell", 500, 100);
    textSize(28);
    text("Edimburgo, 1831 - Glenlair, Reino Unido, 1879",350,130);
    if(giro >= 0 && giro < 90){
      maxwell.stop();
      image(maxwell1,50,50,300,300);
      image(maxwell2,900,250,300,300);
      image(maxwell3,300,450,300,200);
    }else if(giro >= 90 && giro < 180){
      maxwell.stop();
      pushMatrix();
      textSize(26);
      text("Breve Reseña", 560, 160);
      textSize(18);
      text("Físico británico. Nació en el seno de", x1,280);
      text("una familia escocesa de la clase media,",x1,310);
      text(" hijo único de un abogado de Edimburgo. Tras la",x1,340);
      text("temprana muerte de su madre a causa de un cáncer",x2,370);
      text("abdominal, recibió la educación básica en",x2,400);
      text("la Edimburg Academy, bajo la tutela de su tía Jane Cay.,",x2,430);
      popMatrix();
    }else if(giro >= 180 && giro < 270){
      maxwell.stop();
      pushMatrix();
      textSize(26);
      text("Principales Logros", 560, 160);
      textSize(18);
      text(" Aplicó el análisis estadístico a la interpretación",x1,280);
      text("de la teoría cinética de los gases, con",x1,310);
      text("la denominada función de distribución de",x1,340);
      text("Maxwell-Boltzmann, que establece la probabilidad",x1,370);
      text("de hallar una partícula con una determinada velocidad",x2,400);
      text("en un gas ideal diluido y no sometido a campos",x2,430);
      text("de fuerza externos. Justificó las",x2,460);
      text("hipótesis de Avogadro y de Ampère.",x2,490);
      text("Además quw vinculó la teoría eléctrica y",x2,520);
      text("electromagnética en sus famosas 4 ecuaciones",x2,550);
      text("que soan reconocidas a nivel mundial",x2,580);
      popMatrix();
    }else if(giro >= 270 && giro < 360){
      maxwell.play();
      maxwell.read();
      movieEvent(maxwell);
      pushMatrix();
      image(maxwell, tobj.getScreenX(width),tobj.getScreenY(height),displayWidth/2, displayHeight/3);
      fill(255);
      popMatrix();
    }
  } 
} 
} 
